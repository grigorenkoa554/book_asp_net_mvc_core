﻿using Book_ASP_NET_MVC_CORE.Models;
using Book_ASP_NET_MVC_CORE.MyDbContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Book_ASP_NET_MVC_CORE.Repository
{
    public interface IUserRepository : IDisposable
    {
        void Add(User user);
        IEnumerable<User> GetAll();
        User GetById(int id);
        void Delete(int id);
        void Update(User user);
    }
    public class UserRepository : IUserRepository
    {

        public void Add(User user)
        {
            using (var dbContext = new FirstDbContext())
            {
                dbContext.Users.Add(user);
                dbContext.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            using (var dbContext = new FirstDbContext())
            {
                dbContext.Users.Remove(dbContext.Users.First(x => x.Id == id));
                dbContext.SaveChanges();
            }
        }

        public IEnumerable<User> GetAll()
        {
            using (var dbContext = new FirstDbContext())
            {
                return dbContext.Users.ToList();
            }
        }

        public User GetById(int id)
        {
            using (var dbContext = new FirstDbContext())
            {
                return dbContext.Users.First(x => x.Id == id);
            }
        }

        public void Update(User user)
        {
            using (var dbContext = new FirstDbContext())
            {
                var old = dbContext.Users.First(x => x.Id == user.Id);
                if (old!=null)
                {
                    old.FirstName = user.FirstName;
                    old.LastName = user.LastName;
                    old.Age = user.Age;
                }
                dbContext.SaveChanges();
            }
        }
        public void Dispose()
        {
            using (var dbContext = new FirstDbContext())
            {
                dbContext?.Dispose();
            }
        }
    }
}

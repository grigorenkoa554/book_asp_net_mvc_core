﻿using Book_ASP_NET_MVC_CORE.Models;
using Book_ASP_NET_MVC_CORE.MyDbContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Book_ASP_NET_MVC_CORE.Repository
{
    public interface ICatRepository : IDisposable
    {
        void Add(Cat cat);
        IEnumerable<Cat> GetAll();
        Cat GetById(int id);
        void Delete(int id);
        void Update(int id);
    }
    public class CatRepository : ICatRepository
    {

        public void Add(Cat cat)
        {
            using (var dbContext = new FirstDbContext())
            {
                dbContext.Cats.Add(cat);
                dbContext.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            using (var dbContext = new FirstDbContext())
            {
                dbContext.Cats.Remove(dbContext.Cats.First(x => x.Id == id));
                dbContext.SaveChanges();
            }
        }

        public IEnumerable<Cat> GetAll()
        {
            using (var dbContext = new FirstDbContext())
            {
                return dbContext.Cats.ToList();
            }
        }

        public Cat GetById(int id)
        {
            using (var dbContext = new FirstDbContext())
            {
                return dbContext.Cats.First(x => x.Id == id);
            }
        }

        public void Update(int id)
        {
            throw new NotImplementedException();
        }
        public void Dispose()
        {
            using (var dbContext = new FirstDbContext())
            {
                dbContext?.Dispose();
            }
        }
    }
}

﻿using Book_ASP_NET_MVC_CORE.Models;
using Book_ASP_NET_MVC_CORE.Repository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Book_ASP_NET_MVC_CORE.Controllers
{
    public class CatController : Controller
    {
        static readonly List<Cat> cats = new List<Cat>();
        private readonly ICatRepository catRepository;
        public CatController(ICatRepository catRepository)
        {
            this.catRepository = catRepository;
        }
        static CatController()
        {
            cats.Add(new Cat { Id = 2, Name = "Lolik", Color = "Red", Age = 21 });
            cats.Add(new Cat { Id = 3, Name = "Kitty", Color = "Black", Age = 11 });
            cats.Add(new Cat { Id = 4, Name = "Mirchik", Color = "Yelllow", Age = 13 });
        }
        public IActionResult Details()
        {
            var cat = new Cat { Id = 1, Name = "Pusik", Color = "White", Age = 15 };
            return View(cat);
        }

        public string CurrentTime()
        {
            return $"{DateTime.Now}";
        }

        public string MyOwnTime(string str)
        {
            return $"{str} ::: {DateTime.Now}";
        }

        public IActionResult AllCats()
        {
            return View();
        }

        public IEnumerable<Cat> GetAllCats()
        {
            using (catRepository)
            {
                return catRepository.GetAll();
            }
            // return cats;
        }

        [HttpPost]
        public Cat AddCat(Cat cat)
        {
            using(catRepository)
            {
                catRepository.Add(cat);
                return cat;
            }
        }

        [HttpDelete]
        public string DeleteCat(int id)
        {
            using (catRepository)
            {
                catRepository.Delete(id);
                return "Cat was deleted";
            }
        }

        public IActionResult Index()
        {
            return View();
        }
    }
}

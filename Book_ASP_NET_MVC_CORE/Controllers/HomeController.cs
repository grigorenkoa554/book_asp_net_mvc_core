﻿using Book_ASP_NET_MVC_CORE.Models;
using Book_ASP_NET_MVC_CORE.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Book_ASP_NET_MVC_CORE.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private static List<User> users = new List<User>();
        private readonly IUserRepository userRepository;
        public HomeController(IUserRepository userRepository, ILogger<HomeController> logger)
        {
            this.userRepository = userRepository;
            _logger = logger;
        }

        public IActionResult Index()
        {
            _logger.Log(LogLevel.Information, "Some in HomeContorller action Index", DateTime.Now);
            return View();
        }

        public IActionResult Privacy()
        {
            //throw new Exception("Some error");
            _logger.Log(LogLevel.Warning, $"Some in HomeContorller action Privacy: {DateTime.Now}");
            return View();
        }

        public IActionResult MyFirstAction()
        {
            _logger.LogError( $"Some in HomeContorller action MyFirstAction: {DateTime.Now}");
            return View();
        }

        public IActionResult ShowUsers()
        {
            using (userRepository)
            {
                return View(userRepository.GetAll());
                // return View(users);
            }
        }

        public IActionResult Edit(int id)
        {
            using (userRepository)
            {
                var item = userRepository.GetById(id);
                //var item = users.First(x => x.Id == id);
                return View(item);
            }
        }

        [HttpPost]
        public IActionResult Edit(User user)
        {
            using (userRepository)
            {
                //var item = userRepository.GetById(user.Id);
                //var item = users.First(x=>x.Id == user.Id);
                userRepository.Update(user);

                //item.FirstName = user.FirstName;
                //item.LastName = user.LastName;
                //item.Age = user.Age;
                return View("ShowUsers", userRepository.GetAll());
            }            
        }

        public IActionResult Delete(int id)
        {
            using (userRepository)
            {
                var item = userRepository.GetById(id);
                return View(item);
            }
        }

        [HttpPost]
        public IActionResult Delete(User user)
        {
            using (userRepository)
            {
                userRepository.Delete(user.Id);
                return View("ShowUsers", userRepository.GetAll());
            }
            // var item = users.Find(x => x.Id == user.Id);
            //users.Remove(item);
            //return View("ShowUsers", users);
        }

        public IActionResult Create()
        {
            using (userRepository)
            {
                return View();
            }
        }
       
        [HttpPost]
        public IActionResult Create(User user)
        {
            using (userRepository)
            {
                userRepository.Add(user);
                return View("ShowUsers", userRepository.GetAll());
            }
            //var maxId = users.Max(x => x.Id);
            //user.Id = maxId + 1;

            //users.Add(user);
            //return View("ShowUsers", users);
        }

        public IActionResult Details(int id)
        {
            using (userRepository)
            {
                var item = userRepository.GetById(id);
                return View(item);
            }
            //var item = users.First(y => y.Id == id);
            //return View(item);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Book_ASP_NET_MVC_CORE.Models;
using Microsoft.EntityFrameworkCore;

namespace Book_ASP_NET_MVC_CORE.MyDbContext
{
    public class FirstDbContext : DbContext
    {
        public FirstDbContext()
        {
            Database.EnsureCreated();
        }

        public DbSet<Cat> Cats { get; set; }


        public DbSet<User> Users { get; set; }



        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var cs = @"Data Source = 'DESKTOP-IOHQV8L'; 
                Initial Catalog='DbForASPNetMvcCore';
                Integrated Security=true;";
            optionsBuilder.UseSqlServer(cs);
        }
    }
}



﻿using Book_ASP_NET_MVC_CORE.Models;
using Book_ASP_NET_MVC_CORE.Repository;
using System.Collections.Generic;
using System.Linq;

namespace Book_ASP_NET_MVC_CORE.MyDbContext
{
    public interface IDbInitializer
    {
        public void AddIfNotExists();
    }

    public class DbInitializer : IDbInitializer
    {
        private readonly IUserRepository userRepository;
        private readonly ICatRepository catRepository;

        public DbInitializer(IUserRepository userRepository, ICatRepository catRepository)
        {
            this.userRepository = userRepository;
            this.catRepository = catRepository;
        }

        public void AddIfNotExists()
        {
            using (userRepository)
            {
                if (userRepository.GetAll().Count() == 0)
                {
                    var users = new List<User> {
                        new User { FirstName= "Faks1", LastName="Petrov1", Age = 15},
                        new User { FirstName= "Faks2", LastName="Petrov2", Age = 15},
                        new User { FirstName= "Faks3", LastName="Petrov3", Age = 15},
                        new User { FirstName= "Faks4", LastName="Petrov4", Age = 15}
                    };
                    foreach (var item in users)
                    {
                        userRepository.Add(item);
                    }
                }
            }
            using (catRepository)
            {
                if (catRepository.GetAll().Count() == 0)
                {
                    var items = new List<Cat> { 
                    new Cat { Name = "Polushka", Age = 7, Color = "Yellow"},
                    new Cat { Name = "Musik", Age = 5, Color = "Black"},
                    new Cat { Name = "Kusik", Age = 2, Color = "Red"},
                    new Cat { Name = "Givchik", Age = 9, Color = "White"},
                    new Cat { Name = "Kikko", Age = 16, Color = "Gray"}
                    };
                    foreach (var item in items)
                    {
                        catRepository.Add(item);
                    }
                }
            }
        }
    }
}
